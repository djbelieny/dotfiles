export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
# Get readable list of network IPs
alias ips="ifconfig -a | perl -nle'/(\d+\.\d+\.\d+\.\d+)/ && print $1'"
alias myip="dig +short myip.opendns.com @resolver1.opendns.com"
alias flush="dscacheutil -flushcache" # Flush DNS cache
alias ovftool="/Applications/VMware\ OVF\ Tool/ovftool"
alias rmkh="perl -pi -e 's/\Q$_// if ($. == $1);' ~/.ssh/known_hosts"

function parse_git_branch {
   git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

export PS1="\W \$(parse_git_branch)$ "

GRADLE_HOME=/Users/djbelieny/projects/tools/gradle-2.4;
export GRADLE_HOME
export ANDROID_HOME=/Users/djbelieny/android-sdks
export JAVA_HOME=$(/usr/libexec/java_home)
export EC2_HOME="/usr/local/ec2/ec2-api-tools-1.7.5.1"
export TERRAFORM_HOME="~/tools/terraform"
export PATH="$PATH:$ANDROID_HOME:$HOME/.rvm/bin:$JAVA_HOME:$EC2_HOME:$TERRAFORM_HOME" # Add RVM to PATH for scripting
export PATH="/usr/local/bin:$HOME/.rvm/bin:$HOME/rpi/arm-cs-tools/bin:$GRADLE_HOME/bin:$PATH" #Add rpi emulator tools to path

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
